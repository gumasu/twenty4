package com.example.twenty4;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener {
	
	int e = 0;
	int f = 0;
	int g = 0;
	int h = 0;
	int j = 0;
	int k = 0;
	int equ =0;
	int storea=0;
	int storeb=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		((Button) findViewById(R.id.num1)).setOnClickListener(this);
		((Button) findViewById(R.id.num3)).setOnClickListener(this);
		((Button) findViewById(R.id.num7)).setOnClickListener(this);
		((Button) findViewById(R.id.num9)).setOnClickListener(this);

		((Button) findViewById(R.id.add)).setOnClickListener(this);
		((Button) findViewById(R.id.sub)).setOnClickListener(this);
		((Button) findViewById(R.id.mul)).setOnClickListener(this);
		((Button) findViewById(R.id.div)).setOnClickListener(this);

		((Button) findViewById(R.id.ac)).setOnClickListener(this);
		((Button) findViewById(R.id.retry)).setOnClickListener(this);

		((Button) findViewById(R.id.equ)).setOnClickListener(this);
		((Button) findViewById(R.id.button1)).setOnClickListener(this);
		((Button) findViewById(R.id.button2)).setOnClickListener(this);
		((Button) findViewById(R.id.button3)).setOnClickListener(this);
		((Button) findViewById(R.id.button4)).setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	String output = "0";
	int input1 = 0;
	int sign = 0;
	String outputa= "0";
	String outputb= "0";
	void computefunc() {
		int input2 = Integer.parseInt(output);
		int res = 0;
		if (sign == 1) {
			res = input1 + input2;
		} else if (sign == 2) {
			res = input1 - input2;
		} else if (sign == 3) {
			if (input2 == 0) {
				res = input1 * 1;
			} else {
				res = input1 * input2;
			}
		} else if (sign == 4) {
			if (input2 == 0) {
				res = input1 / 1;
			} else {
				res = input1 / input2;
			}
		}
		output = res + "";
		outputa = res + "";
		outputb = res + "";
	}

	@Override
	public void onClick(View v) {
		int num = (int) (Math.random() * 9);
		int num2 = (int) (Math.random() * 9);
		int num3 = (int) (Math.random() * 9);
		int num4 = (int) (Math.random() * 9);
		int a = num + 1;
		int b = num2 + 1;
		int c = num3 + 1;
		int d = num4 + 1;
		String se = String.valueOf(e);
		String see = String.valueOf(storea);
		String seee = String.valueOf(storeb);
		String snum = String.valueOf(a);
		String snum2 = String.valueOf(b);
		String snum3 = String.valueOf(c);
		String snum4 = String.valueOf(d);
		int id = v.getId();
		//TextView textview1 = (TextView) findViewById(R.id.textView1);
		TextView textview2 = (TextView) findViewById(R.id.textView2);
		TextView textview3 = (TextView) findViewById(R.id.textView3);
		TextView tvOutput = (TextView) findViewById(R.id.output);
		TextView op = (TextView) findViewById(R.id.operator);
		TextView but1 = (TextView) findViewById(R.id.num1);
		TextView but3 = (TextView) findViewById(R.id.num3);
		TextView but7 = (TextView) findViewById(R.id.num7);
		TextView but9 = (TextView) findViewById(R.id.num9);
		//textview1.setText(se);
		textview2.setText(see);
		textview3.setText(seee);
		switch (id) {
		case R.id.num1:

			if (e==1 || equ==1){
				break;
			}else if(e==0){
				output += ((Button) v).getText().toString();
				
				if (output.charAt(0) == '0')
					output = output.substring(1);
				
				if (output.length() <= 0)
					output = "0";
				if (e==0){
					e=e+1;
					se = ""+e;
				tvOutput.setText(output);
			//	textview1.setText(se);
				equ=1;
				}
			}
				
			
		break;
		case R.id.num3:

			if (f == 0 && equ==0) {

				output += ((Button) v).getText().toString();

				if (output.charAt(0) == '0')
					output = output.substring(1);

				if (output.length() <= 0)
					output = "0";

				tvOutput.setText(output);
				f=f+1;
				equ=1;
			} 
			
		break;
		case R.id.num7:

			if (g == 0 && equ==0) {

				output += ((Button) v).getText().toString();

				if (output.charAt(0) == '0')
					output = output.substring(1);

				if (output.length() <= 0)
					output = "0";

				tvOutput.setText(output);
				g=g+1;
				equ=1;
			}
			
		break;
		case R.id.num9:
			if (h == 0 && equ==0) {
				
				output += ((Button) v).getText().toString();
				
				if (output.charAt(0) == '0')
					output = output.substring(1);
				
				if (output.length() <= 0)
					output = "0";
				
				tvOutput.setText(output);
				h=h+1;
				equ=1;
			}
				
		break;	
		case R.id.ac:
			input1 = 0;
			sign = 0;
			output = "0";
			op.setText("");
			but1.setText(snum);
			but3.setText(snum2);
			but7.setText(snum3);
			but9.setText(snum4);
			e=0;
			f=0;
			g=0;
			h=0;
			j=0;
			k=0;
			equ=0;
			storea=0;
			storeb=0;
			tvOutput.setText(output);
			break;
			
		case R.id.retry:
			input1 = 0;
			sign = 0;
			output = "0";
			op.setText("");
			e=0;
			f=0;
			g=0;
			h=0;
			j=0;
			k=0;
			equ=0;
			storea=0;
			storeb=0;
			tvOutput.setText(output);
			break;

		case R.id.add:
			if (sign == 0) {
				sign = 1;
				input1 = Integer.parseInt(output);
				output = "0";
				// outputTV.setText(output);
				op.setText("+");
			} else {
				// /////////
				computefunc();
				tvOutput.setText(output);
				// ///////////

				sign = 1;
				input1 = Integer.parseInt(output);
				output = "0";
				// outputTV.setText(output);
				op.setText("+");
				tvOutput.setText(output);
			}
			equ=0;
			break;

		case R.id.sub:
			if (sign == 0) {
				sign = 2;
				input1 = Integer.parseInt(output);
				output = "0";
				// outputTV.setText(output);
				op.setText("-");
			} else {
				// //////////
				computefunc();
				tvOutput.setText(output);
				// ///////

				sign = 2;
				input1 = Integer.parseInt(output);
				output = "0";
				// outputTV.setText(output);
				op.setText("-");
				tvOutput.setText(output);
			}

			equ=0;
			break;

		case R.id.mul:
			if (sign == 0) {
				sign = 3;
				input1 = Integer.parseInt(output);
				output = "0";
				// outputTV.setText(output);
				op.setText("x");
			} else {
				// //////////
				computefunc();
				tvOutput.setText(output);
				// ///////

				sign = 3;
				input1 = Integer.parseInt(output);
				output = "0";
				// outputTV.setText(output);
				op.setText("x");
				tvOutput.setText(output);
			}

			equ=0;
			break;

		case R.id.div:
			if (sign == 0) {
				sign = 4;
				input1 = Integer.parseInt(output);
				output = "0";
				// outputTV.setText(output);
				op.setText("/");
			} else {
				// //////////
				computefunc();
				tvOutput.setText(output);
				// ///////

				sign = 4;
				input1 = Integer.parseInt(output);
				output = "0";
				// outputTV.setText(output);
				op.setText("/");
				tvOutput.setText(output);
			}

			equ=0;
			break;

		case R.id.equ:
			computefunc();
			tvOutput.setText(output);
			op.setText("");
			sign = 0;

			//equ=0;
			
			if(e==1 && f==1 && g==1 && h==1){
				if(Integer.parseInt(output)==24){
					tvOutput.setText("WIN");
				}else{
					tvOutput.setText("LOSE");
				}
				e=0;
				f=0;
				g=0;
				h=0;
			}
			
			
			
			break;
			
		case R.id.button1:
			computefunc();
			tvOutput.setText(outputa);
			storea=storea+Integer.parseInt(outputa);
			output = "0";
			op.setText("");
			input1 = 0;
			sign = 0;
			output = "0";
			see = ""+storea;
			equ=0;
			tvOutput.setText(output);
			textview2.setText(see);
			//equ=0;
			
			
			
			break;
			
		case R.id.button2:
			computefunc();
			tvOutput.setText(outputb);
			storeb=storeb+Integer.parseInt(outputb);
			output = "0";
			op.setText("");
			input1 = 0;
			sign = 0;
			output = "0";
			seee = ""+storeb;
			equ=0;
			tvOutput.setText(output);
			textview3.setText(seee);
			//equ=0;
			
			
			
			break;
			
		case R.id.button3:
			if (j == 0 && equ==0) {
			output += storea;
			if (output.charAt(0) == '0')
				output = output.substring(1);
			
			if (output.length() <= 0)
				output = "0";
			tvOutput.setText(output);
			j=j+1;
			equ=1;
			}
			break;
		case R.id.button4:
			if (k == 0 && equ==0) {
			output += storeb;
			if (output.charAt(0) == '0')
				output = output.substring(1);
			
			if (output.length() <= 0)
				output = "0";
			tvOutput.setText(output);
			k=k+1;
			equ=1;
			}

		}

	}

}